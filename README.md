![Elementary logo](https://kevinrichard.ch/medias/elementary-framework/Elementary-square-150x150.svg)

# Elementary Framework
Elementary is a framework providing basic but extremely flexible reusable HTML & CSS components.

Note that this is an ongoing project, and that some components can still be missing.

### Why Elementary?
This framework aims to provide the elementary blocks to any project that uses the [Atomic Principles](http://atomicdesign.bradfrost.com/table-of-contents/).

## How to use
### Basic usage
Simply clone this repo or download the zip version.
Copy the `elementary-framework/build/elementary-components.min.css` file to your project folder, for instance in your CSS folder.
Then you just have to include this file to your page's `head` section.

Example:

```html
<!DOCTYPE html>
<html>
    <head>
        [...]
        <link rel="stylesheet" type="text/css" href="/path/to/file/elementary-components.min.css">
```

### Advanced usage
Clone this repo directly in your project, following your existing structure.

Example:

```
- MyProject
    |
    +-- css/
    +-- js/
    +-- medias/
    +-- partials/
    +-- scss/
    |   |
    |   +-- elementary-framework/
    |
    +-- index.html
```

Then build the framework with all your other scss files in the your `css/` directory. It should genereate a `css/elementary-components.min.css` or `css/elementary-components.css` file, depending on your configurtion.

#### How to tweak the framework?
The main idea with this framework is to provide some kind of "universal" basic components. But it will clearly miss one important thing: the context of your project. Meaning you will probably need to override these styles to your specific needs, in your specific context. If we see the framework as a top level of information to your components, then that means you will need a lower level –here we call it "app" level– to take over specifc styles.

I strongly recommmand you to use the same structure than the framework. This will helps you follow your future changes. You can also duplicate the framework variables to personalized them.

Example:

```
- MyProject
    |
    +-- css/
    +-- js/
    +-- medias/
    +-- partials/
    +-- scss/
    |   |
    |   +-- app/
    |   |   |
    |   |   +-- components/
    |   |   +-- variables/
    |   |   |
    |   |   +-- app-components.scss
    |   |
    |   +-- elementary-framework/
    |
    +-- index.html
```

Then create a file per component, which contains only the styles that you need to override. Ex. `app/components/_Button.scss` could contains only specific styles for your button's project. Then link all these files, for instance in a `app/[your_app]-components.scss` file:

```sass
/**
 * Elemntary framework dependencies
 */
@import '../elementary-framework/source/variables/_Colors.scss';
@import '../elementary-framework/source/variables/_Spacing.scss';
@import '../elementary-framework/source/variables/_Breakpoints.scss';

/**
 * App dependencies
 */
@import 'variables/_Colors.scss';
@import 'variables/_Fonts.scss';
@import 'variables/_Shared.scss';
@import 'variables/_Whatsinput.scss';

/**
 * Elementary Components Override
 */
@import 'components/_Button.scss';
@import 'components/_Elements.scss';
@import 'components/_Paragraph.scss';
@import 'components/_Wrapper.scss';

/**
 * App Components
 */
@import 'components/_Title.scss';
@import 'components/_Video.scss';
```

Then link it to your page's `head`:

```html
<!DOCTYPE html>
<html>
    <head>
        [...]
        <link rel="stylesheet" type="text/css" href="/path/to/file/elementary-components.min.css">
        <link rel="stylesheet" type="text/css" href="/path/to/file/[your_app]-components.min.css">
```

### Go further on personalisation
The framework use default variables –or tokens– to keep constistency all over the components. For instance, all the spacing (margins and paddings) uses the same set of variables (take a look under `variables/_Spacing.scss`). If you them it will consistently update all the components accordingly.

#### White label components
Another cool thing is that you can change in a second any color or size used by the components. As all the colors are defined in tokens and that the components use directly these token or a variation of them, the components are extremely flexible. Take a look at `variables/_Colors.scss`.